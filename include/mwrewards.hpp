#include <eosio/eosio.hpp>

#include "atomicassets.hpp"
#include "eosio.token.interfaces.hpp"

using namespace eosio;
using namespace std;

static constexpr name COLLECTION_NAME = name("heterogrames");
static constexpr symbol CORE_TOKEN_SYMBOL = symbol("WAX", 8);

CONTRACT mwrewards : public contract
{
public:
  using contract::contract;

  ACTION init();

  using init_action = action_wrapper<"init"_n, &mwrewards::init>;

  [[eosio::on_notify("atomicassets::transfer")]] void receive_atomic_transfer(
    name from, name to, vector<uint64_t> & asset_ids, string memo);

private:
  TABLE rewards_table
  {
    name code;
    asset reward;
    vector<uint32_t> requirements;

    uint64_t primary_key() const { return code.value; }
  };

  typedef multi_index<name("rewards"), rewards_table> rewards_index;

  rewards_index rewards = rewards_index(get_self(), get_self().value);

  void internal_send_reward(name to, asset token, string memo);
};