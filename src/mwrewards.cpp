#include "mwrewards.hpp"

ACTION
mwrewards::init()
{
  require_auth(get_self());
  rewards.emplace(_self, [](auto& _row) {
    _row.code = "season1"_n;
    _row.reward = asset(1000000000, CORE_TOKEN_SYMBOL);
    _row.requirements = {
      140751, 140753, 140754, 140755, 140757, 140758, 140759, 140760, 140761,
      140762, 140763, 140764, 140765, 140767, 140768, 140769, 140770, 140771,
      140772, 140773, 140774, 140775, 140776, 140777, 140778,
    };
  });
}

void
mwrewards::receive_atomic_transfer(name from,
                                   name to,
                                   vector<uint64_t>& asset_ids,
                                   string memo)
{
  require_auth(from);

  if (from == get_self()) {
    return;
  }

  string prefix = "submit:";
  if (memo.rfind(prefix, 0) == 0) {
    auto code = name(memo.substr(prefix.length()));
    auto reward_itr =
      rewards.require_find(code.value, "No reward with this code exists");

    vector<uint32_t> template_ids;

    for (auto asset_id : asset_ids) {
      auto asset_itr =
        atomicassets::get_assets(COLLECTION_NAME)
          .require_find(asset_id, "No asset with this id exists");
      template_ids.push_back(asset_itr->template_id);
    }

    auto requirements = reward_itr->requirements;
    for (auto required_id : requirements) {
      bool is_existed =
        std::find(template_ids.begin(), template_ids.end(), required_id) !=
        template_ids.end();
      check(is_existed, "Unable to perform this action");
    }
    internal_send_reward(from, reward_itr->reward, "success");
  } else {
    check(false, "Unable to perform this action");
  }
}

void
mwrewards::internal_send_reward(name to, asset token, string memo)
{
  action(permission_level{ get_self(), name("active") },
         name("eosio.token"),
         name("transfer"),
         make_tuple(get_self(), to, token, memo))
    .send();
}